/**
* Lina MEHIDI 12211537
* Je déclare qu'il s'agit de mon propre travail.
* Ce travail a été réalisé intégralement par un être humain. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include "comptine_utils.h"

/** 
* Lit des octets dans le fichier de descripteur fd et les met dans buf
* jusqu'au prochain '\n' rencontré, y compris.
* retourne : le nombre d'octets lus avant le premier '\n' rencontré
* précondition : buf doit être assez grand pour contenir les octets lus
* jusqu'au premier caractère '\n' y compris. 
*/
int read_until_nl(int fd, char *buf)
{	int total = -1, n;
	do{	
		if((n=read(fd,buf+total+1,1)) == -1){
			perror("read");	
			exit(2);
		}	
		total+=n;
	}while(n > 0 && buf[total]!='\n');
	return total;
}

/**
* Retourne 1 si nom_fich (chaine de caractère terminée par '\0' se termine par ".cpt". Retourne 0 
* sinon.
*/
int est_nom_fichier_comptine(char *nom_fich)
{
	uint64_t n = strlen(nom_fich); 
	if(n < 4 || strcmp(nom_fich+n-4,".cpt") != 0) return 0;
	return 1;
}

/** Alloue sur le tas et initialise une struct comptine avec le fichier
* de format comptine (extension ".cpt") de nom de base base_name situé dans
* le répertoire dir_name. Le titre et le nom de fichier de la comptine
* retournée sont eux-mêmes alloués sur le tas pour contenir :
* - la première ligne du fichier, avec son '\n', suivi de '\0'
* - une copie de la chaine base_name avec son '\0'
* Retourne : l'adresse de la struct comptine nouvellement créée ou bien
* NULL en cas d'erreur */
struct comptine *init_cpt_depuis_fichier(const char *dir_name, const char *base_name)
{
	if(!est_nom_fichier_comptine(base_name)){
		printf("Le nom n'est pas un nom de fichier .cpt");
		return NULL;
	}
	struct comptine *comptine = malloc(sizeof(struct comptine));
	if(comptine == NULL){
		perror("Malloc");
		return NULL;
	}
	
/*Remplit le nom de fichier.*/
	comptine->nom_fichier = malloc((strlen(base_name)+1)*sizeof(char));
	if(comptine->nom_fichier == NULL){
		perror("Malloc");
		return NULL;
	}
	strcpy(comptine->nom_fichier, base_name);
	
/*Remplit le titre de la comptine.*/
	char *buffer=malloc(sizeof(char) * (strlen(dir_name)+strlen(base_name)+2)); 
	/*On doit concaténer dir_name, '/', et base_name dans une chaine de caractère...*/
	strcpy(buffer,dir_name);
	strcat(buffer, "/");
	strcat(buffer, base_name);
	/*...qu'on va faire passer en argument pour ouvrir le fichier correspondant.*/
	int fd = open(buffer, O_RDONLY);
	if(fd == -1){
		perror("Fichier non existant.");
		return NULL;
	}
	free(buffer);
	char tmp;
	int total = -1, n;
	do{	
		if((n=read(fd,&tmp,1)) == -1){
			perror("read");	
			return NULL;
		}	
		total+=n;
	}while(n > 0 && tmp!='\n');
	comptine->titre = malloc((total+2)*sizeof(char));
	if(comptine->titre == NULL){
		perror("Malloc");
		return NULL;
	}
	lseek(fd,0,SEEK_SET);
	read_until_nl(fd,comptine->titre);
	close(fd);
	comptine->titre[total+1] = '\0';
	return comptine;
}

/** Libère toute la mémoire associée au pointeur de comptine cpt */
void liberer_comptine(struct comptine *cpt)
{
	free(cpt->titre);
	free(cpt->nom_fichier);
	free(cpt);
	cpt = NULL;
}

/** Alloue sur le tas un nouveau catalogue de comptines en lisant les fichiers
* de format comptine (ceux dont le nom se termine par ".cpt") contenus dans le
* répertoire de nom dir_name.
* retourne : un pointeur vers une struct catalogue dont :
* - nb est le nombre de fichiers comptine dans dir_name
* - tab est un tableau de nb pointeurs de comptines, avec pour chacunes
* + nom_fichier égal au nom de base du fichier comptine correspondant
* + titre égal à la première ligne du fichier
* retourne NULL en cas d'erreur. */
struct catalogue *creer_catalogue(const char *dir_name)
{
	struct catalogue *catalogue = malloc(sizeof(struct catalogue));
	catalogue->tab = malloc(100 * sizeof(struct comptine *));
	if(catalogue == NULL){
		perror("Malloc");
		return NULL;
	}
	
	/*Ouvrir un flux répertoire correspondant au répertoire entré en paramètre.*/
	DIR *rd = opendir(dir_name);
	if(rd == NULL){
		perror("Open directory failed.");
		return NULL;
	}
	/*Compter le nombre de fichiers et créer une struct comptine de chaque fichier puis la mettre dans le catalogue.*/
	struct dirent * fichier;
	uint16_t totalFichiers=0;
	while((fichier=readdir(rd)) != NULL){
		if(est_nom_fichier_comptine(fichier->d_name)){ 
			catalogue->tab[totalFichiers++] = init_cpt_depuis_fichier(dir_name, fichier->d_name);
			if(catalogue->tab[totalFichiers-1] == NULL){
				fprintf(stderr,"Créer une comptine à partir du fichier nommé %s d'inode %ld n'a pas marché.", fichier->d_name, fichier->d_ino);
				return NULL;
			}
		}
	}
	closedir(rd);
	
	catalogue->nb = totalFichiers;
	return catalogue;
}

/** Libère toutes les ressources associées à l'adresse c et c lui-même */
void liberer_catalogue(struct catalogue *c)
{
	for(int i=0; i< c->nb; i++){
		liberer_comptine(c->tab[i]);
	}
	free(c->tab);
	free(c);
	c=NULL;
}
