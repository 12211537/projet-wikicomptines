/**
* Lina MEHIDI 12211537
* Je déclare qu'il s'agit de mon propre travail.
* Ce travail a été réalisé intégralement par un être humain. 
**/

/* fichiers de la bibliothèque standard */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
/* bibliothèque standard unix */
#include <unistd.h> /* close, read, write */
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h> /*pthread_create, pthread_join*/
#include <dirent.h>
#include <errno.h>
/* spécifique à internet */
#include <arpa/inet.h> /* inet_pton */
/* spécifique aux comptines */
#include "comptine_utils.h"

#define PORT_WCP 4321

struct catalogue_client{
	int client;/*La socket connectée au client à servir.*/
	struct catalogue *cat;/*L'adresse du catalogue courant.*/
	char *directory;/*Répertoire dans lequel trouver les comptines.*/
	struct sockaddr_in *addr_client;/*Pointeur vers l'adresse IPv4 du client.*/
	pthread_mutex_t *write_lock;/*Mutex permettant d'écrire dans log sans concurrence.*/
};

void usage(char *nom_prog)
{
	fprintf(stderr, "Usage: %s repertoire_comptines\n"
			"serveur pour WCP (Wikicomptine Protocol)\n"
			"Exemple: %s comptines\n", nom_prog, nom_prog);
}
/** Retourne en cas de succès le descripteur de fichier d'une socket d'écoute
 *  attachée au port port et à toutes les adresses locales. */
int creer_configurer_sock_ecoute(uint16_t port);

/** Écrit dans le fichier de desripteur fd la liste des comptines présents dans
 *  le catalogue c comme spécifié par le protocole WCP, c'est-à-dire sous la
 *  forme de plusieurs lignes terminées par '\n' :
 *  chaque ligne commence par le numéro de la comptine (son indice dans le
 *  catalogue) commençant à 0, écrit en décimal, sur 6 caractères
 *  suivi d'un espace
 *  puis du titre de la comptine
 *  trois sauts de ligne finissent le message. */
void envoyer_liste(int fd, struct catalogue *c);

/** Lit dans fd un entier sur 2 octets écrit en network byte order
 *  retourne : cet entier en boutisme machine. */
uint16_t recevoir_num_comptine(int fd);

/** Écrit dans fd la comptine numéro ic du catalogue c dont le fichier est situé
 *  dans le répertoire dirname comme spécifié par le protocole WCP, c'est-à-dire :
 *  chaque ligne du fichier de comptine est écrite avec son '\n' final, y
 *  compris son titre, deux lignes vides terminent le message */
void envoyer_comptine(int fd, const char *dirname, struct catalogue *c, uint16_t ic);

/** Fonction qui prend en paramètre une structure qui contient l'adresse d'une socket
*   liée au client n°i ainsi que l'adresse du catalogue courant de comptines.
*   Elle va envoyer au client la liste des titres de comptine du catalogue puis recevra de sa part un 
*   numéro de l'index et lui enverra la comptine correspondante.*/
void *traiter_client(void *x);

/** Fonction qui va prendre en paramètre un descripteur de fichier, une adresse de type IPv4, une chaine de 
*   caractère décrivant la requete qui a été reçue par le client correspondant à l'adresse en question, puis écrira
*   dans fd une ligne de la forme : "<adresse IPv4> : <description de la requete>\n". Elle prend aussi en paramètre 
*   l'adresse d'un mutex pour le lock et unlock pendant les writes.
*   La description doit ne par contenir de saut de ligne à sa fin.*/
void écrire_log(int fd, struct sockaddr_in *addr_client, char *buf, pthread_mutex_t *write_lock);

int main(int argc, char *argv[])
{
	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}	
	
	int listening = creer_configurer_sock_ecoute(PORT_WCP);
	
	socklen_t len = sizeof(struct sockaddr_in);
	
	int *client = malloc(sizeof(int) * 128); /*La longueur maximale de liste d'attente ...*/
	pthread_t *th = malloc(sizeof(pthread_t) * 128);  /*...est définie dans l'appel à listen est 128.*/
	struct sockaddr_in *adresse_client = malloc(128*sizeof(struct sockaddr_in));
	
	struct catalogue_client *tmp;
	
	/*Initialise le mutex de l'écriture en log.*/
	pthread_mutex_t log_lock ;
	pthread_mutex_init(&log_lock, NULL);

	
	/*Créer le catalogue à partir du répertoire comptines dans le répertoire courant de la fonction.*/
	struct catalogue *catalogue = creer_catalogue(argv[1]);
	
	int i, j=0;
	
	/*Boucle infinie qui sert au plus 128 clients à la fois, avec un thread par client.*/
	while(1){
		for(i=0;i<128;i++){
			if(j>0){/*Si la boucle for a déjà finit d'itérer une fois,..*/
				pthread_join(th[i],NULL); /*..on a déjà utilisé tous les threads au moins une 
				fois. On attend donc qu'ils terminent pour pouvoir continuer à les utiliser.*/
			}
				/*Reçoit des requetes clients et les lègue à un nouveau thread à chaque 
				fois.*/
				client[i] = accept(listening, (struct sockaddr *)adresse_client+i, &len);
				if(client[i] == -1){
					perror("Accept.");
					exit(4);
				}
				
				/*Crée la structure qui contiendra toutes les informations dont aura besoin 
				le thread pour servir correctement le client qui vient de faire un requete 
				de connexion.*/
				tmp = malloc(sizeof(struct catalogue_client));
				tmp->client = client[i];
				tmp->cat = catalogue;
				tmp->directory = argv[1];
				tmp->addr_client = adresse_client+i;
				tmp->write_lock = &log_lock;
				
				
				/*Lance l'interaction entre le thread et le client.*/
				if((th[i]=pthread_create(th+i, NULL, traiter_client, tmp)) != 0){
					perror("pthread_create");
					exit(5);
				}
		}
		j++;	
	}
	/*Libère l'espace mémoire occupé par le catalogue.*/
	liberer_catalogue(catalogue);
	
	/*Join tous les threads s'il y en a qui n'ont pas terminé. Ne devrait jamais en arriver là.*/
	for(i=0; i<128; i++){
		pthread_join(th[i], NULL);
	}
	
	/*Détruit le mutex précédemment créé.*/
	pthread_mutex_destroy(&log_lock);
	
	/*Libère l'espace alloué sur le tas au tableau de clients , adresses et threads.*/
	free(adresse_client);
	adresse_client=NULL;
	free(client);
	client=NULL;
	free(th);
	th=NULL;
	
	/*Libère l'espace alloué sur le tas pour les structures passée en paramètre de traiter_client.*/
	free(tmp);
	tmp=NULL;
	
	/*Ferme le socket d'écoute.*/
	close(listening);
	
	return 0;
}


void *traiter_client(void *x)
{	
	/*Récupérer la socket liée au client, l'adresse du catalogue, l'indice des clients pris en charge et le mutex de ce dernier.*/
	struct catalogue_client *tmp = x;
	
	char *file_name = malloc(4*sizeof(char));
	strcpy(file_name, "log");
	
	/*Crée le fichier log du répertoire courant s'il n'existe pas puis l'ouvre en mode append.*/
	int fd = open(file_name, O_WRONLY | O_CREAT, 0644);
	if(fd == -1){
		perror("open");
		exit(2);
	}
		
	/*Protocole applicatif*/	
	/*Envoie ce catalogue au client connecté au serveur.*/
	envoyer_liste(tmp->client,tmp->cat);
	
	/*Note dans log que le client demande la liste de comptines.*/
	char *buf = malloc(sizeof(char)*100);
	strcpy(buf, "Envoi de la liste des comptines.");
	écrire_log(fd, tmp->addr_client, buf,tmp->write_lock);
	
	
	/*Reçoit un indice et envoie la comptine correspondante en boucle infinie, jusqu'à recevoir 
	le nombre de comptines du catalogue en paramètre, ce qui met fin à la connexion.*/
	while(1){	
		/*Recevoir le numéro de comptine à afficher.*/
		uint16_t ic = recevoir_num_comptine(tmp->client);
			
		/*Interrompt la connexion si nb reçu.*/	
		if(ic == (tmp->cat->nb)) {
			/*Note que le client a interrompu la connexion.*/
			strcpy(buf, "Interruption de la connexion.");
			écrire_log(fd, tmp->addr_client, buf,tmp->write_lock);
			break;	
		}
		
		/*Note que le client a demandé le comptine d'indice ic.*/
		sprintf(buf, "Envoi de la comptine d'indice ""%" PRIu16".", ic);
		écrire_log(fd, tmp->addr_client, buf,tmp->write_lock);	
			
		/*Envoyer au client la comptine de numéro ic.*/
		envoyer_comptine(tmp->client, tmp->directory, tmp->cat, ic);
	}
	/*Ferme le fichier après avoir fini d'écrire.*/	
	close(fd);
	
	/*Fermer la socket consacrée à ce client.*/
	close(tmp->client);
	
	/*Free les chaines de caractère utilisées pour enregistrer les logs.*/
	free(buf);
	buf=NULL;
	free(file_name);
	file_name=NULL;
	
	/*Libère l'espace alloué sur le tas pour la structure passée en paramètre.*/
	free(tmp);
	tmp=NULL;

	return NULL;
}

int creer_configurer_sock_ecoute(uint16_t port)
{
	int listening_sock = socket(AF_INET, SOCK_STREAM, 0);
	if(listening_sock == -1){
		perror("Socket.");
		exit(2);
	}
	struct sockaddr_in addr_serv = {
		.sin_family = AF_INET,
		.sin_port = htons(port),
		.sin_addr.s_addr = htonl(INADDR_ANY)
	};

	/*Pour faire que le port soit réutilisable directement après avoir close la socket.*/
	int opt = 1;//1 enables and 0 disables the getting around the time_wait in bind.
	setsockopt(listening_sock, SOL_SOCKET,SO_REUSEADDR, &opt, sizeof(int));

	/*Faire que cette socket n'écoute que sur le port et l'adresse indiqués dans la struct sockaddr passée en paramètre.*/
	if(bind(listening_sock, (struct sockaddr *) &addr_serv, sizeof(addr_serv)) == -1){
		perror("Bind.");
		exit(4);
	}

	/*Faire que cette socket soit celle à travers laquelle les demande de connexion TCP arrivent.*/
	if(listen(listening_sock, 128) == -1){
		perror("Listen.");
		exit(4);
	}
	return listening_sock;
}

void envoyer_liste(int fd, struct catalogue *c)
{
	int i, n;
	n = (c->nb >= 65535 ? 65535 : c->nb);
	for(i=0; i<n; i++){
		dprintf(fd,"%6d %s",i,c->tab[i]->titre);
	}
	dprintf(fd,"\n");
}

uint16_t recevoir_num_comptine(int fd)
{
	uint16_t num_comptine;
	if(read(fd, &num_comptine, sizeof(uint16_t)) == -1){
		perror("Read.");
		exit(2);	
	}
 	return ntohs(num_comptine);
}	

void envoyer_comptine(int fd, const char *dirname, struct catalogue *c, uint16_t ic)
{
	char *buffer=malloc(sizeof(char) * (strlen(dirname)+strlen(c->tab[ic]->nom_fichier)+2));
	
	/*On doit concaténer dirname, '/', et nom_fichier de la comptine numéro i dans une chaine de caractère...*/
	strcpy(buffer,dirname);
	strcat(buffer, "/");
	strcat(buffer, c->tab[ic]->nom_fichier);
	
	/*...qu'on va faire passer en argument pour ouvrir le fichier correspondant.*/
	int cd = open(buffer, O_RDONLY);
	if(cd == -1){
		perror("Fichier non existant.");
		exit(2);
	}
	free(buffer);
	
	char *buf = malloc(sizeof(char)*1024);
	
	int n=1;
	while(n>0){
		if((n = read(cd, buf, 1024)) == -1){
			perror("Read.");
			exit(2);	
		}
		if(n==0) break;
		if(write(fd, buf, n) == -1){
			perror("Write.");
			exit(2);
		}
	}
	
	/*Ajout de deux '\n' en plus de celui compris dans la dernière ligne pour que le client 
	distingue la fin de fichier.*/
	buf[0]='\n';
	buf[1]='\n';
	if(write(fd, buf, 2) == -1){
		perror("Write.");
		exit(2);
	}
	

	free(buf);
	buf = NULL;
	
	close(cd);/*Ferme le fichier après avoir fini d'écrire.*/
}

void écrire_log(int fd, struct sockaddr_in *addr_client, char *buf, pthread_mutex_t *write_lock)
{	
	char *addr = malloc(sizeof(*addr_client));/*La chaine de caractère où..*/
	if(inet_ntop(AF_INET, addr_client, addr, sizeof(*addr_client))==NULL){/*..transcrire l'adresse du client en format binaire.*/
		perror("inet_ntop");
		exit(1);
	}
	
	/*Construit la taille  de l'output vers log en incluant la taille de l'adresse, le " : " 	
	et le '\n' de fin.*/
	size_t length = strlen(buf) + strlen(addr) + 4;

	/*Mettre le message à écrire dans une chaine de caractère.*/
	char *msg_log = malloc(length*sizeof(char));
	strcpy(msg_log, addr);
	strcat(msg_log, " : ");
	strcat(msg_log, buf);
	msg_log[length-1]='\n';/*On le set à '\n' pour ne pas lire le caractère '\0' de fin.*/
	
	size_t total = 0, n=0;
	pthread_mutex_lock(write_lock);
	
	/*Met à jour la position de la tete d'écriture dans le cas où un autre processus a écrit sur le fichier entre temps.*/
	lseek(fd, 0, SEEK_END);
	while(total < length){
		n=write(fd, msg_log+total, length-total);
		if((n==0) | (n==-1)){
			perror("write");
			pthread_mutex_unlock(write_lock);
			exit(2);
		}
		total+=n;
	}
	pthread_mutex_unlock(write_lock);
	
	free(addr);
	addr=NULL;
	
	free(msg_log);
	msg_log=NULL;

}



