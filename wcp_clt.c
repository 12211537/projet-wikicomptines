/* Lina MEHIDI 12211537
Je déclare qu'il s'agit de mon propre travail.
Ce travail a été réalisé intégralement par un être humain. */

/* fichiers de la bibliothèque standard */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
/* bibliothèque standard unix */
#include <unistd.h> /* close, read, write */
#include <sys/types.h>
#include <sys/socket.h>
/* spécifique à internet */
#include <arpa/inet.h> /* inet_pton */
/* spécifique aux comptines */
#include "comptine_utils.h"

#define PORT_WCP 4321

void usage(char *nom_prog)
{
	fprintf(stderr, "Usage: %s addr_ipv4\n"
			"client pour WCP (Wikicomptine Protocol)\n"
			"Exemple: %s 208.97.177.124\n", nom_prog, nom_prog);
}

/** Retourne (en cas de succès) le descripteur de fichier d'une socket
 *  TCP/IPv4 connectée au processus écoutant sur port sur la machine d'adresse
 *  addr_ipv4 */
int creer_connecter_sock(char *addr_ipv4, uint16_t port);

/** Lit la liste numérotée des comptines dans le descripteur fd et les affiche
 *  sur le terminal.
 *  retourne : le nombre de comptines disponibles */
uint16_t recevoir_liste_comptines(int fd);

/** Demande à l'utilisateur un nombre entre 0 (compris) et nc (non compris) s'il souhaite afficher 	
 *  une comptine et nc s'il souhaite interrompre la connexion avec le serveur
 *  Retourne la valeur saisie. */
uint16_t saisir_num_comptine(uint16_t nb_comptines);

/** Écrit l'entier ic dans le fichier de descripteur fd en network byte order. */
void envoyer_num_comptine(int fd, uint16_t nc);

/** Affiche la comptine arrivant dans fd sur le terminal, s'interrompt quand la fonction lit trois '\n' consécutifs.*/
void afficher_comptine(int fd);

int main(int argc, char *argv[])
{
	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}
	
	fprintf(stdout, "Bienvenue sur WikiComptines. Vous verrez ci-dessous une liste des comptines disponibles dans notre répertoire.\n - Entrez l'indice d'une comptine pour l'afficher,\n - Entrez le nombre de comptines si vous souhaitez quitter. Amusez-vous bien!\n\n");
	
	int server = creer_connecter_sock(argv[1], PORT_WCP);
	
	/*Protocole applicatif.*/
	/*Demande au serveur de lui envoyer les titres des comptines dans son catalogue.*/
	uint16_t i = recevoir_liste_comptines(server), j;
	while(1){
		/*Demande à l'utilisateur de saisir un numéro de comptine à afficher.*/
		j = saisir_num_comptine(i);
		
		fprintf(stdout,"\nComptine n°""%"PRIu16" :\n", j);
		/*Envoie une requete au serveur pour qu'il envoie le contenu de la comptine numéro i.*/
		envoyer_num_comptine(server, j);
		
		if(j == i){
			break;
		}
		
		/*Affiche sur la sortie standard le contenu envoyé par le serveur.*/
		afficher_comptine(server);
	}
	
	fprintf(stdout,"C'était très amusant. Au revoir!\n");
	
	/*Ferme la socket.*/
	close(server);
	return 0;
}

int creer_connecter_sock(char *addr_ipv4, uint16_t port)
{
	int server = socket(AF_INET, SOCK_STREAM, 0);
	if(server == -1){
		perror("Socket.");
		exit(2);
	}
	struct sockaddr_in ad_server = {
		.sin_family = AF_INET ,
		.sin_port = htons(port) 
	};
	if(inet_pton(AF_INET, addr_ipv4, &ad_server.sin_addr) != 1){
		perror("Cette adresse IPv4 n'existe pas.");
		exit(1);
	}
	if(connect(server, (struct sockaddr *)&ad_server, sizeof(ad_server)) == -1){
		perror("Connect : le serveur n'est peut etre pas en mesure de prendre une requete.");
		exit(3);
	}
	return server;
}

uint16_t recevoir_liste_comptines(int fd)
{
	char buf, tmp;
	int n, total = 0;
	while(1){
		tmp=buf;
		if((n=read(fd, &buf, 1)) == -1){
			perror("Read.");
			exit(2);
		}
		if(n==0) break;
		if(write(1, &buf, n) == -1){
			perror("Write.");
			exit(2);
		}
		if(buf == '\n'){
			if(tmp=='\n') break;
			total++;
		}	
	}
	
	return total;
}

uint16_t saisir_num_comptine(uint16_t nb_comptines)
{
    uint16_t num;
    printf("Veuillez entrer un entier compris entre 0 et %d si vous souhaitez afficher la comptine correspondante, tapez %d si vous souhaitez interrompre la connexion avec le serveur : ", nb_comptines-1, nb_comptines);
    scanf("%" SCNu16, &num);
    while (num < 0 || num > nb_comptines)
    {
        printf("Numéro invalide.");
        scanf("%" SCNu16, &num);
    }
    return num;
}

void envoyer_num_comptine(int fd, uint16_t nc) 
{
	uint16_t num = htons(nc);
	if(write(fd, &num, sizeof(num))==-1){
    		perror("Write.");
		exit(2);
	}
}

void afficher_comptine(int fd)
{
	char tmp='a', tmp1='a', tmp2='a';
	int n;
	while(1){
		tmp1=tmp;
		
		if((n=read(fd, &tmp, 1)) == -1){
			perror("Read.");
			exit(2);
		}
		
		/*La fin d'écriture de comptine étant 3 sauts de 	
		ligne pour la différencier des simples lignes vides entre strophes...*/
		if(n==0 || (tmp=='\n' && tmp1=='\n')){ /*..On s'assure que si deux sauts de ligne consécutifs arrivent..*/ 
			read(fd, &tmp2, 1);/*..et que le caractère qui les suit en est aussi..*/
			if(tmp2 == '\n') break;/*..alors on termine avant d'écrire les deux derniers \n.*/
			write(1,&tmp,1);
			write(1,&tmp2,1);
			tmp=tmp2;/*Pour faire que tmp redevienne la dernière valeur courante lue.*/
		}
		else{ 
			if(write(1, &tmp, 1)==-1){
				perror("Write.");
				exit(2);
			}
		}	
	}
}



