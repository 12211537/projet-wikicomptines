# Projet Wikicomptines

## Name
WikiComptines application.

## Description
An application in which you have to execute the server on one side and the client(s) on the other.
 As the client, you receive a list of the available nursery rhymes and the index corresponding to each. You can then ask the server for a nursery rhyme in the "comptines" repositiory by typing in the index corresponding to it. You can treat with the server for as many times as you wish. To indicate to the server that you wish to end the connexion, you can send the number of nursery rhymes as an index. Every client connexion is kept in a record file called "log" which will be created and then progressively filled by the server, keeping track of the clients' IP adresses and the nature of each of their requests in the following manner : "IPv4 adress : description of the request.\n".
This WikiComptines protocol  uses a TCP transportation protocol over IPv4.


## Usage
Launch the server on a separate terminal by executing "./wcp_srv comptines" after compiling all the files with the "make" command. You can then launch your desired amount of clients by executing "./wcp_clt 127.0.0.1". You can, of course, replace the loopback adress 127.0.0.1 with any IPv4 adress in your local network so long as you've launched the server on the corresponding machine.

## Support
Contact me on mehidilyna1501@gmail.com or lina.mehidi@edu.univ-paris13.fr .

## Authors and acknowledgment
My teacher, M. Pierre Rousselin wrote the Makefile and .h files whereas I wrote everything else.

## Project status
Finished.
